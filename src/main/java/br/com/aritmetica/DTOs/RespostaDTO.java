package br.com.aritmetica.DTOs;

public class RespostaDTO {

    private double resultado;

    public RespostaDTO() { }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }

}
