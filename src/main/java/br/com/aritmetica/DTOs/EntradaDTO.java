package br.com.aritmetica.DTOs;

import java.util.ArrayList;
import java.util.List;

public class EntradaDTO {
    private List<Double> numeros;

    public EntradaDTO() { }

    public List<Double> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Double> numeros) {
        this.numeros = numeros;
    }
}
