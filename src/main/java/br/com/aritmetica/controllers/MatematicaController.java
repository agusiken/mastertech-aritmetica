package br.com.aritmetica.controllers;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    private boolean nMaiorZero;
    private boolean ehDouble;

    private boolean validaNumeros(@RequestBody EntradaDTO entradaDTO) {
        nMaiorZero = true;
        ehDouble = false;

        for (int i = 0; i < entradaDTO.getNumeros().size(); i++) {
            if (entradaDTO.getNumeros().get(i) < 0) {
                nMaiorZero = false;
            }

            if ((entradaDTO.getNumeros().get(i) % 1.0) != 0.0) {
                ehDouble = true;
            }
        }

        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros");
        } else {
            if (!nMaiorZero || ehDouble) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie apenas numeros naturais!");
            }
        }

        return nMaiorZero;
    }

    private void validaDivisao(@RequestBody EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() > 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Para divisao apenas 2 numeros.");
        }
        if(entradaDTO.getNumeros().get(1) == 0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não há divisão por número 0!");
        }

    }

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO) {

        validaNumeros(entradaDTO);

        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO) {

        validaNumeros(entradaDTO);

        RespostaDTO respostaSubtracao = matematicaService.subtracao(entradaDTO);
        return respostaSubtracao;
    }

    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO) {
        validaNumeros(entradaDTO);
        validaDivisao(entradaDTO);

        RespostaDTO respostaDivisao = matematicaService.divisao(entradaDTO);
        return respostaDivisao;
    }

    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO) {
        validaNumeros(entradaDTO);

        RespostaDTO respostaMultiplicacao = matematicaService.multiplicacao(entradaDTO);
        return respostaMultiplicacao;
    }
}