package br.com.aritmetica.service;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO) {
        int numero = 0;

        for (double n : entradaDTO.getNumeros()) {
            numero += (int)n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO) {
        int numero = 0;

        for (double n : entradaDTO.getNumeros()) {
            numero =- (int)n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;

    }

    public RespostaDTO divisao(EntradaDTO entradaDTO) {
        double numero = 1;

        numero = entradaDTO.getNumeros().get(0) / entradaDTO.getNumeros().get(1);
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;

    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO) {
        int numero = 1;

        for (double n : entradaDTO.getNumeros()) {
            numero *= n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }
}
